/* eslint-disable node/no-unpublished-require */
const assert = require('assert');
const { testHelper, marsTime } = require('..');
const { generate100Calendars } = require('..');
const { generateCalendars } = require('../lib/generate-calendars');
const { utcToZonedTime } = require('date-fns-tz');
// const { getCalendar } = require('../lib/get-calendar');
// const { Collection } = require('lokijs');

describe('mars-time', () => {
  beforeEach('install calendars', async function () {
    // load the calendars, clearing the collection before loading
    const calendars = await generateCalendars(2016, 2065);
    testHelper(calendars, true);
  });

  it('load calendars without clearing the collection', async () => {
    testHelper(await generateCalendars(2021, 2023));

    // Need to fake the time so it's correct for local timezone
    const utcDate = utcToZonedTime('2022-03-20T00:00:00+00:00', 'Africa/Accra');

    const { year } = await marsTime(utcDate);
    assert.strictEqual(year, 2022);
  });

  // test generateCalendars inputs
  it('generateCalendars() - throws and error if starting year < 2016', async () => {
    try {
      await generateCalendars(2014);
    } catch (error) {
      assert.deepStrictEqual(
        error,
        'The starting calendar year must be 2016 or later'
      );
    }
  });

  it('generateCalendars() defaults to year 2016', async () => {
    const results = await generateCalendars();

    const { year, begin, end } = results[0];

    assert.strictEqual(results.length, 1, 'only 1 calendar created by default');
    assert.strictEqual(year, 2016, 'year is 2016');
    assert.strictEqual(begin, '2016-01-03');
    assert.strictEqual(end, '2016-12-31');
  });

  it('generate calendar for FY 2020', async () => {
    const results = await generateCalendars(2020);

    const { year, begin, end } = results[0];

    assert.strictEqual(results.length, 1, 'only 1 calendar created by default');
    assert.strictEqual(year, 2020);
    assert.strictEqual(begin, '2019-12-29');
    assert.strictEqual(end, '2021-01-02');
  });

  it('generate calendar for FY 2062', async () => {
    const results = await generateCalendars(2062);

    const { year, begin, end } = results[0];

    assert.strictEqual(results.length, 1, 'only 1 calendar created by default');
    assert.strictEqual(year, 2062);
    assert.strictEqual(begin, '2062-01-01');
    assert.strictEqual(end, '2062-12-30');
  });

  it('generate a 100 year calendar using defaults - current year - 1', async () => {
    const results = await generate100Calendars(null);

    assert.ok(results, 'Calendars was successfully created');

    // Need to fake the time so it's correct for local timezone
    let utcDate = utcToZonedTime('2020-03-20T00:00:00+00:00', 'Africa/Accra');

    const { year: yearStart } = await marsTime(utcDate);
    assert.strictEqual(yearStart, 2020);

    try {
      utcDate = utcToZonedTime('2120-03-20T00:00:00+00:00', 'Africa/Accra');

      // eslint-disable-next-line no-unused-vars
      const { year } = await marsTime(utcDate);
    } catch (error) {
      assert.strictEqual(error.message, 'Calendar not found for the date');
    }

    utcDate = utcToZonedTime('2119-03-20T00:00:00+00:00', 'Africa/Accra');
    const { year: yearEnd } = await marsTime(utcDate);
    assert.strictEqual(yearEnd, 2119);
  });

  it('generates a 100 year calendar starting at 2040', async () => {
    let utcDate;
    const results = await generate100Calendars(2040, true);

    assert.ok(results, 'Calendars was successfully created');

    try {
      utcDate = utcToZonedTime('2039-03-20T00:00:00+00:00', 'Africa/Accra');
      // eslint-disable-next-line no-unused-vars
      const { year } = await marsTime(utcDate);
    } catch (error) {
      assert.strictEqual(error.message, 'Calendar not found for the date');
    }

    utcDate = utcToZonedTime('2040-03-20T00:00:00+00:00', 'Africa/Accra');
    const { year: yearStart } = await marsTime(utcDate);
    assert.strictEqual(yearStart, 2040);

    try {
      utcDate = utcToZonedTime('2140-03-20T00:00:00+00:00', 'Africa/Accra');

      // eslint-disable-next-line no-unused-vars
      const { year } = await marsTime(utcDate);
    } catch (error) {
      assert.strictEqual(error.message, 'Calendar not found for the date');
    }

    utcDate = utcToZonedTime('2139-03-20T00:00:00+00:00', 'Africa/Accra');
    const { year: yearEnd } = await marsTime(utcDate);
    assert.strictEqual(yearEnd, 2139);
  });

  it('date uses current date if date is not provided', async () => {
    const currentYear = new Date().getFullYear();

    const { year } = await marsTime();
    assert.strictEqual(year, currentYear, 'Correct Mars year');
  });

  it('Non ISO date strings generate an error - marstime()', async () => {
    testHelper(await generateCalendars(2021, 2023));

    // prettier-ignore
    try {
      // eslint-disable-next-line no-unused-vars
      const { year } = await marsTime('test');
    } catch (error) {
      assert.strictEqual(error.message, 'Date specified is not a valid date object');
    }
  });

  it('date test without time', async () => {
    const utcDate = utcToZonedTime('2018-08-28T00:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 9, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 35, 'Correct Mars week');
    assert.strictEqual(day, 'B', 'Correct Mars weekday');
    assert.strictEqual(shift, 1, 'Correct Mars shift');
    assert.strictEqual(batch, '835B', 'Correct batch code');
  });

  it('creation date test with time', async () => {
    const utcDate = utcToZonedTime('2018-09-13T14:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 10, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 37, 'Correct Mars week');
    assert.strictEqual(day, 'D', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '837D', 'Correct batch code');
  });

  // first day of a period
  it('first day of a period (P8), 11-7 shift, post day change', async () => {
    const utcDate = utcToZonedTime('2018-07-15T00:30:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 8, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 29, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 1, 'Correct Mars shift');
    assert.strictEqual(batch, '828G', 'Correct batch code');
  });

  // first day of a period
  it('first day of a period (P8), 7-3 shift', async () => {
    const utcDate = utcToZonedTime('2018-07-15T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 8, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 29, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '828G', 'Correct batch code');
  });

  // first day of a period
  it('first day of a period (P8), 3-11 shift', async () => {
    const utcDate = utcToZonedTime('2018-07-15T17:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 8, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 29, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 3, 'Correct Mars shift');
    assert.strictEqual(batch, '828G', 'Correct batch code');
  });

  // first day of a period
  it('first day of a period (P8), betwen 23:00-24:00 day changed', async () => {
    const utcDate = utcToZonedTime('2018-07-15T23:30:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 8, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 29, 'Correct Mars week');
    assert.strictEqual(day, 'A', 'Correct Mars weekday');
    assert.strictEqual(shift, 1, 'Correct Mars shift');
    assert.strictEqual(batch, '829A', 'Correct batch code');
  });

  // first day of a period
  it('first day of a period (P8), 11-7 shift', async () => {
    const utcDate = utcToZonedTime('2018-07-15T03:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 8, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 29, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 1, 'Correct Mars shift');
    assert.strictEqual(batch, '828G', 'Correct batch code');
  });

  it('time past 23:00 should be the next production day', async () => {
    const utcDate = utcToZonedTime('2018-08-28T21:02:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 9, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 35, 'Correct Mars week');
    assert.strictEqual(day, 'B', 'Correct Mars weekday');
    assert.strictEqual(shift, 3, 'Correct Mars shift');
    assert.strictEqual(batch, '835B', 'Correct batch code');
  });

  // still next day - intial logic used 23:30 not 23:00 - verify we haven't regressed
  it('verify logic does not readjust production day after 23:30 - regression test', async () => {
    const utcDate = utcToZonedTime('2018-08-28T21:34:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 9, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 35, 'Correct Mars week');
    assert.strictEqual(day, 'B', 'Correct Mars weekday');
    assert.strictEqual(shift, 3, 'Correct Mars shift');
    assert.strictEqual(batch, '835B', 'Correct batch code');
  });

  it('it should return P12', async () => {
    const utcDate = utcToZonedTime('2018-11-19T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 12, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 47, 'Correct Mars week');
    assert.strictEqual(day, 'A', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '847A', 'Correct batch code');
  });

  it('should return P13', async () => {
    const utcDate = utcToZonedTime('2018-12-22T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 51, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '851F', 'Correct batch code');
  });

  it('P13 Week 5, should return P13', async () => {
    const utcDate = utcToZonedTime('2020-12-30T06:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2020, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 5, 'Correct week in period');
    assert.strictEqual(week, 53, 'Correct Mars week');
    assert.strictEqual(day, 'C', 'Correct Mars weekday');
    assert.strictEqual(shift, 1, 'Correct Mars shift');
    assert.strictEqual(batch, '053C', 'Correct batch code');
  });

  it('should return 2019 because it is the next fiscal year', async () => {
    const utcDate = utcToZonedTime('2018-12-31T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 1, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 1, 'Correct Mars week');
    assert.strictEqual(day, 'A', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '901A', 'Correct batch code');
  });

  it('treats time as 00:00:00 so fiscal year is 2019', async () => {
    const utcDate = utcToZonedTime('2018-12-31T00:00:00+00:00', 'Africa/Accra');
 
    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 1, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 1, 'Correct Mars week');
    assert.strictEqual(day, 'A', 'Correct Mars weekday');
    assert.strictEqual(shift, 1, 'Correct Mars shift');
    assert.strictEqual(batch, '901A', 'Correct batch code');
  });

  // ============================================================
  // Production calendar days start  at 11:00 pm, so anything
  // after 23:00 is the next production day

  // last Saturday in fiscal year 2018, before 23:00
  it('last day FY2018 before 23:00 year 2018, day F', async () => {
    const utcDate = utcToZonedTime('2018-12-29T22:59:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 52, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 3, 'Correct Mars shift');
    assert.strictEqual(batch, '852F', 'Correct batch code');
  });

  // last Saturday in fiscal year 2018, after 23:00
  it('last day FY2018 after 23:00 year 2018 , day G', async () => {
    const utcDate = utcToZonedTime('2018-12-29T23:02:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 52, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 1, 'Correct Mars shift');
    assert.strictEqual(batch, '852G', 'Correct batch code');
  });

  // first sunday in fiscal year 2019, before 23:00
  it('first day FY2019 before 23:00 year 2019 , day G', async () => {
    const utcDate = utcToZonedTime('2018-12-30T22:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 1, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 1, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 3, 'Correct Mars shift');
    assert.strictEqual(batch, '852G', 'Correct batch code');
  });

  // first sunday in fiscal year 2019, after 23:00
  it('first day FY2019 after 23:00 year 2019 , day A', async () => {
    const utcDate = utcToZonedTime('2018-12-30T23:05:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 1, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 1, 'Correct Mars week');
    assert.strictEqual(day, 'A', 'Correct Mars weekday');
    assert.strictEqual(shift, 1, 'Correct Mars shift');
    assert.strictEqual(batch, '901A', 'Correct batch code');
  });

  // last Saturday in fiscal year 2020, after 23:00
  it('last day FY2020 after 23:00 year 2020 , day G, 53 week year', async () => {
    const utcDate = utcToZonedTime('2021-01-02T23:02:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2020, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 5, 'Correct week in period');
    assert.strictEqual(week, 53, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 1, 'Correct Mars shift');
    assert.strictEqual(batch, '053G', 'Correct batch code');
  });

  // first sunday in fiscal year 2020, before 23:00
  it('first day FY2021 before 23:00 year 2021 , day G', async () => {
    const utcDate = utcToZonedTime('2021-01-03T22:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2021, 'Correct Mars year');
    assert.strictEqual(period, 1, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 1, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 3, 'Correct Mars shift');
    assert.strictEqual(batch, '053G', 'Correct batch code');
  });

  // first sunday in fiscal year 2021, after 23:00
  it('first day FY2021 after 23:00 year 2021 , day A', async () => {
    const utcDate = utcToZonedTime('2021-01-03T23:05:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2021, 'Correct Mars year');
    assert.strictEqual(period, 1, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 1, 'Correct Mars week');
    assert.strictEqual(day, 'A', 'Correct Mars weekday');
    assert.strictEqual(shift, 1, 'Correct Mars shift');
    assert.strictEqual(batch, '101A', 'Correct batch code');
  });

  // ===============================================================

  it('Failing time??', async () => {
    const utcDate = utcToZonedTime('2018-08-30T16:10:44+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 9, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 35, 'Correct Mars week');
    assert.strictEqual(day, 'D', 'Correct Mars weekday');
    assert.strictEqual(shift, 3, 'Correct Mars shift');
    assert.strictEqual(batch, '835D', 'Correct batch code');
  });

  it('still P1, 2019', async () => {
    const utcDate = utcToZonedTime('2019-01-04T12:00:00+00:00', 'Africa/Accra');
 
    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 1, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 1, 'Correct Mars week');
    assert.strictEqual(day, 'E', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '901E', 'Correct batch code');
  });

  it('52 week year, P13', async () => {
    const utcDate = utcToZonedTime('2019-12-28T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 52, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '952F', 'Correct batch code');
  });

  it('53 week year, P13W4', async () => {
    const utcDate = utcToZonedTime('2020-12-23T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2020, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 52, 'Correct Mars week');
    assert.strictEqual(day, 'C', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '052C', 'Correct batch code');
  });

  it('52 week year, P13W4', async () => {
    const utcDate = utcToZonedTime('2019-12-25T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 52, 'Correct Mars week');
    assert.strictEqual(day, 'C', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '952C', 'Correct batch code');
  });

  it('53 week year, P13W5D2', async () => {
    const utcDate = utcToZonedTime('2020-12-28T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2020, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 5, 'Correct week in period');
    assert.strictEqual(week, 53, 'Correct Mars week');
    assert.strictEqual(day, 'A', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '053A', 'Correct batch code');
  });

  it('53 week year, P13W5D7, last day of FY', async () => {
    const utcDate = utcToZonedTime('2020-12-28T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2020, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 5, 'Correct week in period');
    assert.strictEqual(week, 53, 'Correct Mars week');
    assert.strictEqual(day, 'A', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '053A', 'Correct batch code');
  });

  it('the far future, 2050', async () => {
    const utcDate = utcToZonedTime('2050-12-28T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2050, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 52, 'Correct Mars week');
    assert.strictEqual(day, 'C', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '052C', 'Correct batch code');
  });

  it('even farther into the future, 2065', async () => {
    const utcDate = utcToZonedTime('2066-01-02T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2065, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 5, 'Correct week in period');
    assert.strictEqual(week, 53, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '553F', 'Correct batch code');
  });

  it('FAIL - Too far in the future, no calendars', async () => {
    const utcDate = utcToZonedTime('2075-12-28T12:00:00+00:00', 'Africa/Accra');

    try {
      // prettier-ignore
      // eslint-disable-next-line no-unused-vars
      const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);
    } catch (error) {
      assert(error, 'Too far in the future');
      return;
    }
  });

  it('P4W2 2018', async () => {
    const utcDate = utcToZonedTime('2018-04-03T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 4, 'Correct Mars period');
    assert.strictEqual(pweek, 2, 'Correct week in period');
    assert.strictEqual(week, 14, 'Correct Mars week');
    assert.strictEqual(day, 'B', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '814B', 'Correct batch code');
  });

  // testing that we calculate the correct week in period
  it('test for week 1 - P4W1-2018', async () => {
    const utcDate = utcToZonedTime('2018-03-31T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2018, 'Correct Mars year');
    assert.strictEqual(period, 4, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 13, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '813F', 'Correct batch code');
  });

  it('test for week 2 - P7W2-2019', async () => {
    const utcDate = utcToZonedTime('2019-06-27T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 7, 'Correct Mars period');
    assert.strictEqual(pweek, 2, 'Correct week in period');
    assert.strictEqual(week, 26, 'Correct Mars week');
    assert.strictEqual(day, 'D', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '926D', 'Correct batch code');
  });

  it('test for pweek 3 - P9W3-2019', async () => {
    const utcDate = utcToZonedTime('2019-08-31T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 9, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 35, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '935F', 'Correct batch code');
  });

  it('test for week 5 - P13W5-2020', async () => {
    const utcDate = utcToZonedTime('2020-12-28T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2020, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 5, 'Correct week in period');
    assert.strictEqual(week, 53, 'Correct Mars week');
    assert.strictEqual(day, 'A', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '053A', 'Correct batch code');
  });

  // fully test days of week
  it('test for week day 7 = G, batch code should be for previous week', async () => {
    const utcDate = utcToZonedTime('2019-10-06T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 11, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 41, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '940G', 'Correct batch code');
  });

  it('test for week day = A', async () => {
    const utcDate = utcToZonedTime('2019-10-07T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 11, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 41, 'Correct Mars week');
    assert.strictEqual(day, 'A', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '941A', 'Correct batch code');
  });

  it('test for week day = B', async () => {
    const utcDate = utcToZonedTime('2019-10-08T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 11, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 41, 'Correct Mars week');
    assert.strictEqual(day, 'B', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '941B', 'Correct batch code');
  });

  it('test for week day = C', async () => {
    const utcDate = utcToZonedTime('2019-10-09T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 11, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 41, 'Correct Mars week');
    assert.strictEqual(day, 'C', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '941C', 'Correct batch code');
  });

  it('test for week day = D', async () => {
    const utcDate = utcToZonedTime('2019-10-10T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 11, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 41, 'Correct Mars week');
    assert.strictEqual(day, 'D', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '941D', 'Correct batch code');
  });

  it('test for week day 1 = E', async () => {
    const utcDate = utcToZonedTime('2019-10-11T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 11, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 41, 'Correct Mars week');
    assert.strictEqual(day, 'E', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '941E', 'Correct batch code');
  });

  it('test for week day 1 = F', async () => {
    const utcDate = utcToZonedTime('2019-10-12T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 11, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 41, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '941F', 'Correct batch code');
  });

  // validate one year of periods
  // Day G edge case - first day of fiscal year, batch code in previous year
  it('P1 W1 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2018-12-30T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 1, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 1, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '852G', 'Correct batch code');
  });

  it('P1 W2 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-01-06T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 1, 'Correct Mars period');
    assert.strictEqual(pweek, 2, 'Correct week in period');
    assert.strictEqual(week, 2, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '901G', 'Correct batch code');
  });

  it('P1 W3 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-01-13T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 1, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 3, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '902G', 'Correct batch code');
  });

  it('P1 W4 Day 28', async () => {
    const utcDate = utcToZonedTime('2019-01-26T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 1, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 4, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '904F', 'Correct batch code');
  });

  it('P2 W1 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-01-27T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 2, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 5, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '904G', 'Correct batch code');
  });

  it('P2 W2 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-02-03T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 2, 'Correct Mars period');
    assert.strictEqual(pweek, 2, 'Correct week in period');
    assert.strictEqual(week, 6, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '905G', 'Correct batch code');
  });

  it('P2 W3 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-02-10T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 2, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 7, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '906G', 'Correct batch code');
  });

  it('P2 W4 Day 28', async () => {
    const utcDate = utcToZonedTime('2019-02-23T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 2, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 8, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '908F', 'Correct batch code');
  });

  it('P3 W1 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-02-24T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 3, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 9, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '908G', 'Correct batch code');
  });

  it('P3 W2 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-03-03T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 3, 'Correct Mars period');
    assert.strictEqual(pweek, 2, 'Correct week in period');
    assert.strictEqual(week, 10, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '909G', 'Correct batch code');
  });

  it('P3 W3 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-03-10T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 3, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 11, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '910G', 'Correct batch code');
  });

  it('P3 W4 Day 28', async () => {
    const utcDate = utcToZonedTime('2019-03-23T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 3, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 12, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '912F', 'Correct batch code');
  });

  it('P4 W1 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-03-24T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 4, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 13, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '912G', 'Correct batch code');
  });

  it('P4 W2 Day 1', async () => {
    const utcDate = utcToZonedTime('2019-03-31T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 4, 'Correct Mars period');
    assert.strictEqual(pweek, 2, 'Correct week in period');
    assert.strictEqual(week, 14, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '913G', 'Correct batch code');
  });

  it('P4 W3 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-04-07T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 4, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 15, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '914G', 'Correct batch code');
  });

  it('P4 W4 Day 28', async () => {
    const utcDate = utcToZonedTime('2019-04-20T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 4, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 16, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '916F', 'Correct batch code');
  });

  it('P5 W1 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-04-21T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 5, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 17, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '916G', 'Correct batch code');
  });

  it('P5 W2 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-04-28T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 5, 'Correct Mars period');
    assert.strictEqual(pweek, 2, 'Correct week in period');
    assert.strictEqual(week, 18, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '917G', 'Correct batch code');
  });

  it('P5 W3 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-05-05T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 5, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 19, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '918G', 'Correct batch code');
  });

  it('P5 W4 Day 28', async () => {
    const utcDate = utcToZonedTime('2019-05-18T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 5, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 20, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '920F', 'Correct batch code');
  });

  it('P6 W1 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-05-19T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 6, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 21, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '920G', 'Correct batch code');
  });

  it('P6 W2 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-05-26T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 6, 'Correct Mars period');
    assert.strictEqual(pweek, 2, 'Correct week in period');
    assert.strictEqual(week, 22, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '921G', 'Correct batch code');
  });

  it('P6 W3 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-06-02T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 6, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 23, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '922G', 'Correct batch code');
  });

  it('P6 W4 Day 28', async () => {
    const utcDate = utcToZonedTime('2019-06-15T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 6, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 24, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '924F', 'Correct batch code');
  });

  it('P7 W1 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-06-16T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 7, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 25, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '924G', 'Correct batch code');
  });

  it('P7 W2 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-06-23T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 7, 'Correct Mars period');
    assert.strictEqual(pweek, 2, 'Correct week in period');
    assert.strictEqual(week, 26, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '925G', 'Correct batch code');
  });

  it('P7 W3 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-06-30T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 7, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 27, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '926G', 'Correct batch code');
  });

  it('P7 W4 Day 28', async () => {
    const utcDate = utcToZonedTime('2019-07-13T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 7, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 28, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '928F', 'Correct batch code');
  });

  it('P8 W1 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-07-14T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 8, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 29, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '928G', 'Correct batch code');
  });

  it('P8 W2 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-07-21T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 8, 'Correct Mars period');
    assert.strictEqual(pweek, 2, 'Correct week in period');
    assert.strictEqual(week, 30, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '929G', 'Correct batch code');
  });

  it('P8 W3 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-07-28T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 8, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 31, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '930G', 'Correct batch code');
  });

  it('P8 W4 Day 28', async () => {
    const utcDate = utcToZonedTime('2019-08-10T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 8, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 32, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '932F', 'Correct batch code');
  });

  it('P9 W1 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-08-11T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 9, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 33, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '932G', 'Correct batch code');
  });

  it('P9 W2 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-08-18T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 9, 'Correct Mars period');
    assert.strictEqual(pweek, 2, 'Correct week in period');
    assert.strictEqual(week, 34, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '933G', 'Correct batch code');
  });

  it('P9 W3 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-08-25T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 9, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 35, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '934G', 'Correct batch code');
  });

  it('P9 W4 Day 28', async () => {
    const utcDate = utcToZonedTime('2019-09-07T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 9, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 36, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '936F', 'Correct batch code');
  });

  it('P10 W1 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-09-08T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 10, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 37, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '936G', 'Correct batch code');
  });

  it('P10 W2 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-09-15T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 10, 'Correct Mars period');
    assert.strictEqual(pweek, 2, 'Correct week in period');
    assert.strictEqual(week, 38, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '937G', 'Correct batch code');
  });

  // TODO: Verify week
  it('P10 W3 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-09-22T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 10, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 39, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '938G', 'Correct batch code');
  });

  it('P10 W4 Day 28', async () => {
    const utcDate = utcToZonedTime('2019-10-05T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 10, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 40, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '940F', 'Correct batch code');
  });

  it('P11 W1 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-10-06T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 11, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 41, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '940G', 'Correct batch code');
  });

  it('P11 W2 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-10-13T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 11, 'Correct Mars period');
    assert.strictEqual(pweek, 2, 'Correct week in period');
    assert.strictEqual(week, 42, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '941G', 'Correct batch code');
  });

  it('P11 W3 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-10-20T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 11, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 43, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '942G', 'Correct batch code');
  });

  it('P11 W4 Day 28', async () => {
    const utcDate = utcToZonedTime('2019-11-02T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 11, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 44, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '944F', 'Correct batch code');
  });

  it('P12 W1 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-11-03T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 12, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 45, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '944G', 'Correct batch code');
  });

  it('P12 W2 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-11-10T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 12, 'Correct Mars period');
    assert.strictEqual(pweek, 2, 'Correct week in period');
    assert.strictEqual(week, 46, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '945G', 'Correct batch code');
  });

  it('P12 W3 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-11-17T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 12, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 47, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '946G', 'Correct batch code');
  });

  it('P12 W4 Day 28', async () => {
    const utcDate = utcToZonedTime('2019-11-30T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 12, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 48, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '948F', 'Correct batch code');
  });

  it('P13 W1 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-12-01T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 1, 'Correct week in period');
    assert.strictEqual(week, 49, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '948G', 'Correct batch code');
  });

  it('P13 W2 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-12-08T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 2, 'Correct week in period');
    assert.strictEqual(week, 50, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '949G', 'Correct batch code');
  });

  it('P13 W3 Day 1, Day code G - set batch code is for prior week', async () => {
    const utcDate = utcToZonedTime('2019-12-15T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 3, 'Correct week in period');
    assert.strictEqual(week, 51, 'Correct Mars week');
    assert.strictEqual(day, 'G', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '950G', 'Correct batch code');
  });

  it('P13 W4 Day 28', async () => {
    const utcDate = utcToZonedTime('2019-12-28T12:00:00+00:00', 'Africa/Accra');

    // prettier-ignore
    const { year, period, pweek, week, day, shift, batch } = await marsTime(utcDate);

    assert.strictEqual(year, 2019, 'Correct Mars year');
    assert.strictEqual(period, 13, 'Correct Mars period');
    assert.strictEqual(pweek, 4, 'Correct week in period');
    assert.strictEqual(week, 52, 'Correct Mars week');
    assert.strictEqual(day, 'F', 'Correct Mars weekday');
    assert.strictEqual(shift, 2, 'Correct Mars shift');
    assert.strictEqual(batch, '952F', 'Correct batch code');
  });

  // Shift was test in other test but there are shift specific
  it('First shift begining', async () => {
    const utcDate = utcToZonedTime('2019-12-28T23:00:00+00:00', 'Africa/Accra');
    const { shift } = await marsTime(utcDate);

    assert.strictEqual(shift, 1, 'Correct Mars shift');
  });

  it('First shift - middle', async () => {
    const utcDate = utcToZonedTime('2019-12-28T03:00:00+00:00', 'Africa/Accra');
    const { shift } = await marsTime(utcDate);

    assert.strictEqual(shift, 1, 'Correct Mars shift');
  });

  it('First shift - end', async () => {
    const utcDate = utcToZonedTime('2019-12-28T06:59:00+00:00', 'Africa/Accra');
    const { shift } = await marsTime(utcDate);

    assert.strictEqual(shift, 1, 'Correct Mars shift');
  });

  it('Second shift begining', async () => {
    const utcDate = utcToZonedTime('2019-12-28T07:00:00+00:00', 'Africa/Accra');
    const { shift } = await marsTime(utcDate);

    assert.strictEqual(shift, 2, 'Correct Mars shift');
  });

  it('Second shift - middle', async () => {
    const utcDate = utcToZonedTime('2019-12-28T11:00:00+00:00', 'Africa/Accra');
    const { shift } = await marsTime(utcDate);

    assert.strictEqual(shift, 2, 'Correct Mars shift');
  });

  it('Second shift - end', async () => {
    const utcDate = utcToZonedTime('2019-12-28T13:59:00+00:00', 'Africa/Accra');
    const { shift } = await marsTime(utcDate);

    assert.strictEqual(shift, 2, 'Correct Mars shift');
  });

  it('Third shift begining', async () => {
    const utcDate = utcToZonedTime('2019-12-28T15:00:00+00:00', 'Africa/Accra');
    const { shift } = await marsTime(utcDate);

    assert.strictEqual(shift, 3, 'Correct Mars shift');
  });

  it('Third shift - middle', async () => {
    const utcDate = utcToZonedTime('2019-12-28T20:00:00+00:00', 'Africa/Accra');
    const { shift } = await marsTime(utcDate);

    assert.strictEqual(shift, 3, 'Correct Mars shift');
  });

  it('Third shift - end', async () => {
    const utcDate = utcToZonedTime('2019-12-28T22:59:00+00:00', 'Africa/Accra');
    const { shift } = await marsTime(utcDate);

    assert.strictEqual(shift, 3, 'Correct Mars shift');
  });
});
