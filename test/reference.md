Fiscal year calendars beginning and end dates. This data is not used in
the package - it's here for reference only


[
  {"Year":1992,"Begin":"12/29/1991","End":"01/02/1993"},
  {"Year":1993,"Begin":"01/03/1993","End":"01/01/1994"},
  {"Year":1994,"Begin":"01/02/1994","End":"12/31/1994"},
  {"Year":1995,"Begin":"01/01/1995","End":"12/30/1995"},
  {"Year":1996,"Begin":"12/31/1995","End":"12/28/1996"},
  {"Year":1997,"Begin":"12/29/1996","End":"12/27/1997"},
  {"Year":1998,"Begin":"12/28/1997","End":"01/02/1999"},
  {"Year":1999,"Begin":"01/03/1999","End":"01/01/2000"},
  {"Year":2000,"Begin":"01/02/2000","End":"12/30/2000"},
  {"Year":2001,"Begin":"12/31/2000","End":"12/29/2001"},
  {"Year":2002,"Begin":"12/30/2001","End":"12/28/2002"},
  {"Year":2003,"Begin":"12/29/2002","End":"12/27/2003"},
  {"Year":2004,"Begin":"12/28/2003","End":"01/01/2005"},
  {"Year":2005,"Begin":"01/02/2005","End":"12/31/2005"},
  {"Year":2006,"Begin":"01/01/2006","End":"12/30/2006"},
  {"Year":2007,"Begin":"12/31/2006","End":"12/29/2007"},
  {"Year":2008,"Begin":"12/30/2007","End":"12/27/2008"},
  {"Year":2009,"Begin":"12/28/2008","End":"01/02/2010"},
  {"Year":2010,"Begin":"01/03/2010","End":"01/01/2011"},
  {"Year":2011,"Begin":"01/02/2011","End":"12/31/2011"},
  {"Year":2012,"Begin":"01/01/2012","End":"12/29/2012"},
  {"Year":2013,"Begin":"12/30/2012","End":"12/28/2013"},
  {"Year":2014,"Begin":"12/29/2013","End":"12/27/2014"},
  {"Year":2015,"Begin":"12/28/2014","End":"01/02/2016"},
  {"Year":2016,"Begin":"01/03/2016","End":"12/31/2016"},
  {"Year":2017,"Begin":"01/01/2017","End":"12/30/2017"},
  {"Year":2018,"Begin":"12/31/2017","End":"12/29/2018"},
  {"Year":2019,"Begin":"12/30/2018","End":"12/28/2019"},
  {"Year":2020,"Begin":"12/29/2019","End":"01/02/2021"},
  {"Year":2021,"Begin":"01/03/2021","End":"01/01/2022"},
  {"Year":2022,"Begin":"01/02/2022","End":"12/31/2022"},
  {"Year":2023,"Begin":"01/01/2023","End":"12/30/2023"},
  {"Year":2024,"Begin":"12/31/2023","End":"12/28/2024"},
  {"Year":2025,"Begin":"12/29/2024","End":"12/27/2025"},
  {"Year":2026,"Begin":"12/28/2025","End":"01/02/2027"},
  {"Year":2027,"Begin":"01/03/2027","End":"01/01/2028"},
  {"Year":2028,"Begin":"01/02/2028","End":"12/30/2028"},
  {"Year":2029,"Begin":"12/31/2028","End":"12/29/2029"},
  {"Year":2030,"Begin":"12/30/2029","End":"12/28/2030"},
  {"Year":2031,"Begin":"12/29/2030","End":"12/27/2031"},
  {"Year":2032,"Begin":"12/28/2031","End":"01/01/2033"},
  {"Year":2033,"Begin":"01/02/2033","End":"12/31/2033"},
  {"Year":2034,"Begin":"01/01/2034","End":"12/30/2034"},
  {"Year":2035,"Begin":"12/31/2034","End":"12/29/2035"},
  {"Year":2036,"Begin":"12/30/2035","End":"12/27/2036"},
  {"Year":2037,"Begin":"12/28/2036","End":"01/02/2038"},
  {"Year":2038,"Begin":"01/03/2038","End":"01/01/2039"},
  {"Year":2039,"Begin":"01/02/2039","End":"12/31/2039"},
  {"Year":2040,"Begin":"01/01/2040","End":"12/29/2040"},
  {"Year":2041,"Begin":"12/30/2040","End":"12/28/2041"},
  {"Year":2042,"Begin":"12/29/2041","End":"12/27/2042"},
  {"Year":2043,"Begin":"12/28/2042","End":"01/02/2044"},
  {"Year":2044,"Begin":"01/03/2044","End":"12/31/2044"},
  {"Year":2045,"Begin":"01/01/2045","End":"12/30/2045"},
  {"Year":2046,"Begin":"12/31/2045","End":"12/29/2046"},
  {"Year":2047,"Begin":"12/30/2046","End":"12/28/2047"},
  {"Year":2048,"Begin":"12/29/2047","End":"01/02/2049"},
  {"Year":2049,"Begin":"01/03/2049","End":"01/01/2050"},
  {"Year":2050,"Begin":"01/02/2050","End":"12/31/2050"},
  {"Year":2051,"Begin":"01/01/2051","End":"12/30/2051"},
  {"Year":2052,"Begin":"12/31/2051","End":"12/28/2052"},
  {"Year":2053,"Begin":"12/29/2052","End":"12/27/2053"},
  {"Year":2054,"Begin":"12/28/2053","End":"01/02/2055"},
  {"Year":2055,"Begin":"01/03/2055","End":"01/01/2056"},
  {"Year":2056,"Begin":"01/02/2056","End":"12/30/2056"},
  {"Year":2057,"Begin":"12/31/2056","End":"12/29/2057"},
  {"Year":2058,"Begin":"12/30/2057","End":"12/28/2058"},
  {"Year":2059,"Begin":"12/29/2058","End":"12/27/2059"},
  {"Year":2060,"Begin":"12/28/2059","End":"01/01/2061"},
  {"Year":2061,"Begin":"01/02/2061","End":"12/31/2061"},
  {"Year":2062,"Begin":"01/01/2062","End":"12/30/2062"},
  {"Year":2063,"Begin":"12/31/2062","End":"12/29/2063"},
  {"Year":2064,"Begin":"12/30/2063","End":"12/27/2064"},
  {"Year":2065,"Begin":"12/28/2064","End":"01/02/2066"}
]
