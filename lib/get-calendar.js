const getYear = require('date-fns/getYear');
const isWithinInterval = require('date-fns/isWithinInterval');
const format = require('date-fns/format');
const parseISO = require('date-fns/parseISO');

//----------------------------------
// get the FY calendar for the date
//----------------------------------
const getCalendar = async (date, collection) => {
  // verify the date is javascript date object
  if (Object.prototype.toString.call(date) != '[object Date]') {
    throw 'Date specified is not a valid date object';
  }

  const yr = getYear(date);

  if (isNaN(yr)) throw 'Invalid date';

  // get the calendars on either side of date year
  const results = collection.find({ year: { $between: [yr - 1, yr + 1] } });

  if (!results.length) {
    throw 'No calendars returned';
  }

  const dateOnly = parseISO(format(date, 'yyyy-MM-dd'));

  // prettier-ignore
  const calendar = results.filter((cal) => isWithinInterval(dateOnly, {start: parseISO(cal.begin), end: parseISO(cal.end)}));

  if (!calendar.length) throw 'Calendar not found for the date';
  else return calendar[0];
};

module.exports.getCalendar = getCalendar;
