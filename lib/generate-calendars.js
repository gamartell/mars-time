const isLeapYear = require('date-fns/isLeapYear');
const isFriday = require('date-fns/isFriday');
const isThursday = require('date-fns/isThursday');
const addDays = require('date-fns/addDays');
const format = require('date-fns/format');
const parseISO = require('date-fns/parseISO');

// 2015 is zero year for determining fiscal calendars
// no dates prior to 2016 can be processed
const FY2015 = 2015;

// last day of business FY 2015
const yearEndFY2015 = '2016-01-02';

// 13 periods * 28 days
const fiscalYearDays = 364;

// generate an array of business calendars with FY start and end dates
//
// [
//   {
//    year: year,
//    begin: 'start date',
//    end: 'end date'
//   },
//    ...
// ]
//

const generateCalendars = async (startYear = 2016, endYear) => {
  let calStartDate, calEndDate, december31, leap;
  let hasWeek53 = false;
  const calendars = [];

  if (startYear < 2016) {
    throw 'The starting calendar year must be 2016 or later';
  }

  // // only produce one FY calendar
  if (!endYear) endYear = startYear;

  // always start at the end of FY2015
  calEndDate = parseISO(yearEndFY2015);

  // year end FY2015 is the known point in time
  // to start generating the calendars
  for (let year = FY2015; year < endYear; year++) {
    hasWeek53 = false;

    // first day of next fiscal year
    calStartDate = addDays(calEndDate, 1);

    // is this a leap year?
    december31 = parseISO(`${year + 1}-12-31`);
    leap = isLeapYear(december31);

    // does the year have 53 weeks?
    if (isThursday(december31)) hasWeek53 = true;
    if (leap && isFriday(december31)) hasWeek53 = true;

    // end date of current calendar
    calEndDate = addDays(calStartDate, fiscalYearDays - 1);

    // if year has 53 weeks, add 7 days
    if (hasWeek53) calEndDate = addDays(calEndDate, 7);

    // only push calendars in specified range
    if (year + 1 >= startYear)
      calendars.push({
        year: year + 1,
        begin: `${format(calStartDate, 'yyyy-MM-dd')}`,
        end: `${format(calEndDate, 'yyyy-MM-dd')}`
      });
  }

  return calendars;
};

module.exports.generateCalendars = generateCalendars;
