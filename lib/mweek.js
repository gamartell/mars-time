const differenceInCalendarWeeks = require('date-fns/differenceInWeeks');
const parseISO = require('date-fns/parseISO');

async function marsWeek(date, cal) {
  return differenceInCalendarWeeks(date, parseISO(cal.begin)) + 1;
}

module.exports.marsWeek = marsWeek;
