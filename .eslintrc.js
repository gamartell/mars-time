module.exports = {
  env: {
    commonjs: true,
    es6: true,
    browser: false,
    mocha: true,
    node: true
  },
  extends: [
    'eslint:recommended',
    'plugin:mocha/recommended',
    'plugin:node/recommended',
    'prettier'
  ],
  plugins: ['mocha', 'promise'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaVersion: 2018
  },
  rules: {
    // eslint
    'arrow-body-style': [
      'warn',
      'as-needed',
      { requireReturnForObjectLiteral: true }
    ],
    'eol-last': ['error', 'always'],
    camelcase: ['warn', { ignoreDestructuring: true, allow: [] }],
    'implicit-arrow-linebreak': ['warn', 'beside'],
    'no-else-return': 'warn',
    'no-lonely-if': 'warn',
    'no-sync': 'warn',
    'no-unneeded-ternary': 'warn',
    'prefer-const': [
      'error',
      {
        destructuring: 'any',
        ignoreReadBeforeAssign: false
      }
    ],
    'id-blacklist': ['error', 'data', 'err', 'e', 'cb', 'callback'],

    // eslint mocha
    'mocha/no-mocha-arrows': 0,

    // eslint node
    'node/exports-style': ['error', 'module.exports'],
    'node/file-extension-in-import': ['error', 'always'],
    'node/prefer-global/buffer': ['error', 'always'],
    'node/prefer-global/console': ['error', 'always'],
    'node/prefer-global/process': ['error', 'always'],
    'node/prefer-global/url-search-params': ['error', 'always'],
    'node/prefer-global/url': ['error', 'always'],
    'node/prefer-promises/dns': 'error',
    'node/prefer-promises/fs': 'error',
    'node/no-extraneous-require': [
      'error',
      {
        allowModules: ['body-parser']
      }
    ]
  }
};
