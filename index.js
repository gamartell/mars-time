const addDays = require('date-fns/addDays');
const differenceInDays = require('date-fns/differenceInDays');
const getHours = require('date-fns/getHours');
const getDay = require('date-fns/getDay');
const parseISO = require('date-fns/parseISO');
const loki = require('lokijs');

const { getCalendar } = require('./lib/get-calendar');
const { generateCalendars } = require('./lib/generate-calendars');
const { marsWeek } = require('./lib/mweek');

// intialize in-memory calendars database
const db = new loki('calendars.db');
const collection = db.addCollection('calendars', { indices: ['year'] });

//---------------------------------------------------
// Generate and load a 100 year calendar into LokiJs
//---------------------------------------------------
const generate100Calendars = async (startYear, clearCollection = false) => {
  if (!startYear) startYear = new Date().getFullYear() - 1;

  try {
    const results = await generateCalendars(startYear, startYear + 99);

    // clear loki collection?
    if (clearCollection) collection.clear({ removeIndices: true });

    await collection.insert(results);

    return true;
  } catch (error) {
    return false;
  }
};

//------------------------------------------------
// Determine Mars year, period, period week, year
// week and day for the date
//------------------------------------------------
const marsTime = async (date = new Date()) => {
  let cal,
    isWeek53 = false, // 53rd week
    pweek, // period week
    codeWeek, // date code week
    codeYear; // date code year

  // find fiscal year calendar for the date
  try {
    // verify the date is javascript date object
    // https://stackoverflow.com/questions/643782/how-to-check-whether-an-object-is-a-date
    if (Object.prototype.toString.call(date) != '[object Date]') {
      throw 'Date specified is not a valid date object';
    }

    cal = await getCalendar(date, collection);
  } catch (error) {
    throw new Error(error);
  }

  //===========================================
  // Determine fiscal calendar data
  // period, week and period week
  //===========================================

  // how many days are we into the calendar year?
  const calendarDays = differenceInDays(date, parseISO(cal.begin)) + 1;

  // is day of the fiscal calendar year
  const isFirstDayOfYear = calendarDays == 1;

  // end of week 52 or week 53?
  const isLastDayOfYear = calendarDays == 364 || calendarDays == 371;

  // first day would be 0, but needs
  // to be 1 so we have a fraction
  const fractionalPeriod = calendarDays / 28;

  // more than 13 periods = week 53
  if (fractionalPeriod > 13) {
    isWeek53 = true;
  }

  // Mars period
  let period = Math.ceil(fractionalPeriod);

  // handle week 53 edge case
  if (period > 13) period = 13;

  // Mars Period Week
  // days remaining in the current period
  const daysRemaining = calendarDays - Math.floor(fractionalPeriod) * 28;

  // determine period week
  if (!isWeek53) {
    if (daysRemaining == 0) {
      pweek = 4;
    } else if (daysRemaining <= 7) {
      pweek = 1;
    } else if (daysRemaining <= 14) {
      pweek = 2;
    } else if (daysRemaining <= 21) {
      pweek = 3;
    } else if (daysRemaining > 21) {
      pweek = 4;
    }
  } else {
    pweek = 5;
  }

  // Mars calendar year
  const year = cal.year;

  // Mars Calendar Week
  const week = await marsWeek(date, cal);

  //===========================================
  // Determine production calendar data
  // day code, shift and batch code
  //===========================================

  // Mars Day

  // Production schedule day codes
  // Monday is 'A', Sunday is 'G'
  const dayArray = ['G', 'A', 'B', 'C', 'D', 'E', 'F'];

  let dayNum = getDay(date);

  // production calendar days start at 23:00
  // if hour is between 23:00 and 24:00 bump
  // to the next day
  const hour = getHours(date);

  if (hour >= 23 && dayNum !== 6) dayNum++;
  // reset to 0 if day is F
  else if (hour >= 23 && dayNum == 6) dayNum = 0;

  const day = dayArray[dayNum];

  // Mars Shift

  // shift: 1, start: 23:00 end: 6:59
  // shift: 2, start: 7:00, end: 2:59
  // shift: 3, start: 15:00, end: 22:59
  const shift = hour == 23 || hour < 7 ? 1 : hour >= 7 && hour < 15 ? 2 : 3;

  // =======================================================================
  // Mars batch code - year-week-day ie 2019 week 22, Day F = 922F
  //
  // Edge cases if day code = 'G'
  //
  // 1. Day G is actually the Sunday in the previous week - the batch code
  // needs to set back one week
  //
  // 2. Day G falls on the first day of the fiscal year
  // - the batch code needs to roll back to the last week of previous year
  // and the year part needs to be changed
  //
  // 3. Day G falls on last day of the fiscal year
  // treat the same as any other day
  //
  // i.e. date code for Day G for P1 Day 1 2018 would need to be 752G
  //
  // =======================================================================

  const isDayG = day == 'G';

  // is this first day of calendar year?
  // const isFirstDayOfYear = period == 1 && daysRemaining == 1;

  // any day but G - use current week/year
  if (!isDayG) {
    codeWeek = week;
    codeYear = year;
  }

  // Edge case 3 - Day code G and last day of the year
  if (isDayG && isLastDayOfYear) {
    codeWeek = week;
    codeYear = year;
  }

  // Edge case 1 - Day code G and not first or last days
  if (isDayG && !isFirstDayOfYear && !isLastDayOfYear) {
    // go back one week in the same year
    codeWeek = week - 1;
    codeYear = year;
  }

  // Edge case 2 - Day code G and the first day
  // of the year need to adjsut both week and year
  // this is not pretty...
  if (isDayG && isFirstDayOfYear) {
    // get the last date in previous year
    date = addDays(date, -1);

    // find calendar for last year
    try {
      cal = await getCalendar(date, collection);
    } catch (error) {
      throw new Error(error);
    }

    // assign updated week and year
    codeWeek = await marsWeek(date, cal);
    codeYear = cal.year;
  }

  // =======================================================================
  // =======================================================================

  // build batch code string
  const batch = `${codeYear.toString().charAt(3)}${codeWeek
    .toString()
    .padStart(2, '0')}${day}`;

  return { year, period, pweek, week, day, shift, batch };
};

//-------------------------------------------------
// Loads calendars into the database for testing
//-------------------------------------------------
const testHelper = async (calendars, clearCollection = false) => {
  if (clearCollection) collection.clear({ removeIndices: true });
  try {
    collection.insert(calendars);
    return true;
  } catch (error) {
    return false;
  }
};

module.exports.generateCalendars = generateCalendars;
module.exports.generate100Calendars = generate100Calendars;
module.exports.testHelper = testHelper;
module.exports.marsTime = marsTime;
