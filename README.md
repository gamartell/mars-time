# mars-time

This package provides fiscal and production calendar data based on a specific date/time.

## Fiscal year calendar
The fiscal year uses a 52-53 week calendar nominally running from January to December <sup>1</sup>. The calendar is broken down into thirteen four week periods (28 days) totaling 364 days (13 x 28 days = 364).
The first day of the week for the fiscal calendar is Sunday.

The calendar starts on Sunday and ends on the Saturday nearest the end of December (12/31). The calendar is corrected every few years to compensate for the accumulating days (364 vs 365) and leap years.

The corrected calendar will have a fifth week added to the thirteenth period (P13W5) resulting in the 53 week calendar year.
<br>

The values for `year`, `period`, `week` and `pweek` are based on the fiscal calendar.

<sup>1</sup> The start and end dates for 52-53 week calendars can start outside the boundaries of calendar years due to truncated days and leap years. As an example, FY2015 starts on `12/28/2014` and ends on `01/02/2016`


## Production calendar
The production calendar follows the fiscal calendar but the first day of the week is Monday. Additionally, the production calendar day changes occur at `23:00`, not `24:00`.

The differences in the production calendar affect the determination of the `day`, `shift` and `batch code` values. The `mars-time` module is time aware and uses the time to correctly calculate calendar data.

The comparisons below use FY2000 as an example the year runs from January 2, 2000 to December 30, 2000.

---
 
>#### FY Period 1 Weeks
>
>|Week | Start Date | Designator
>|:-----:|------------|:-----------:
>|1    | Jan 2      | P1W1
>|2    | Jan 9      | P1W2
>|3    | Jan 16     | P1W3
>|4    | Jan 23     | P1W4
>
><br>
>
>#### Period 1, Week 1 Days
>
>| 1/2  | 1/3  | 1/4  | 1/5  | 1/6  | 1/7  | 1/8  |
>|:-----|:----:|:----:|:----:|:----:|:----:|:----:|
>| P1D1 | P1D2 | P1D3 | P1D4 | P1D5 | P1D6 | P1D7 |

><br>
<br>

>#### Production Period 1 Weeks
>| Week | Start Date |
>|:----:|------------|
>|  1   | Jan 3      |
>|  2   | Jan 10     |
>|  3   | Jan 17     |
>|  4   | Jan 24     |
>
><br>
>
>#### Period 1, Week 1 (01) Days
> Days codes A through G are assigned to each production day
>
>|      | 1/3  | 1/4  | 1/5  | 1/6  | 1/7  | 1/8  | 1/9  |
>|:-----|:----:|:----:|:----:|:----:|:----:|:----:|:----:|
>| Day  |  A   |  B   |  C   |  D   |  E   |  F   |  G   |
>| Code | 001A | 001B | 001C | 001D | 001E | 001F | 001G |
>
>In this example, the production code for 1/2/2000 actually falls into 
>fiscal year 1999. It would have day code `G` and the date code `952G`
>
><br>
>
>#### Week 2 (02) Days
>
>|      | 1/10 | 1/11 | 1/2  | 1/13 | 1/14 | 1/15 | 1/16 |
>|:-----|:----:|:----:|:----:|:----:|:----:|:----:|:----:|
>| Day  |  A   |  B   |  C   |  D   |  E   |  F   |  G   |
>| Code | 002A | 002B | 002C | 002D | 002E | 002F | 002G |
>
><br>
<br>


---
## Requirements

* NodeJS v10 or greater

## Dependencies

*  [date-fns](https://date-fns.org) - Date utility library
*  [LokiJS](https://github.com/techfort/LokiJS) - Embeddable in memory document database

## Installation

The `mars-time` module is not published to `npm` and needs to be installed directly from the repo.  `git` is required to pull the package during installation.

Using npm:

```bash
npm install https://gamartell@bitbucket.org/gamartell/mars-time.git
```

Using Yarn:

```bash
yarn add https://gamartell@bitbucket.org/gamartell/mars-time.git
```

## Usage

The calendar functions need fiscal year starting and ending dates to calculate the data for a specific date/time. The data needs to be provided as an array of JSON objects.

```javascript
[
  {
    year: 2018,
    begin: '12-29-2017',
    end: '12-29-2018'
  },
  {
    year: 2019,
    begin: '12-30-2018',
    end: '12-28-2019'
  },
  {
    year: 2020,
    begin: '12-29-2019',
    end: '1-2-2021'
  }
]
```
Since fiscal calendars can extend into the previous or next calendar years, calendar data is required for the years before and after the test date. The example data above could calculate the period data for a date in `FY2019` but not 2018 or 2020. (see code comments for specifics)

While calendar data can be hardcoded or stored in an application's database, `mars-time` uses an in-memory document database to hold the calendar data. This allows applications to use the functions without having to manage calendar data.

 `mars-time` provides helper functions to dynamically generate calendar data and cache the data in the database. The document in-memory database uses [LokiJS](https://github.com/techfort/LokiJS).

### Functions
The package exports four functions

Function                | Description
------------------------|-------------------------------------------------------------------
generateCalendars       | Generates an array of calendars based on starting and ending years
generate100Calendars | Generate and cache 100 years of calendars based on starting year
marsTime                | Returns specific calendar data for the specified date
<br>


<br>

### __generateCalendars__

`generateCalendars(startYear, endYear)` 

Generates a range of fiscal calendar data . `startYear` and `endYear` are the years to generate the calendars. If `startYear` is not specified it will default to 2016. If `endYear` is not specified it defaults to `startYear`.

Once the calendar data has been loaded into the database it can be reused as needed.

```javascript
const { generateCalendars } = require('mars-time');
const { loadCalendars } = require('mars-time');

async function addCalendars() {
  // generate calendar data for years 2017 to 2025
  const calendars = await generateCalendars(2017, 2025);

  // load into database
  const success = await loadCalendars(calendars);
 
  if (success) {
    // do something
  }
}

```
<br>

### __generate100Calendars__

`generate100Calendars(startYear, clearCollection = false)`

This helper function generates the data for 100 calendar years and loads the database. `startYear` is starting year for the range. If `startYear` is not specified it will default to 2016, creating calendars for 2016 to 2119.

Returns `true` if successful, otherwise `false`.

`clearCollection` is optional and will clear the database before loading new calendar data.

```js
const { generate100Calendars } = require('mars-time');

async function addCalendars(){

  // add 100 years of calendars starting in 2035
  const success = await generate100Calendars(2035);

  if (success) {
    // do something
  }
}
```

If the application uses realtime data and stores the results you can automatically update the calendar data each time your the application starts. The example creates 100 years of calendars starting with the current year. Every time the application is restarted it will create a new data minimizing future maintenance.

```javascript
const { generate100Calendars } = require('mars-time');

async function populateCalendars() {
  return generate100Calendars(new Date().getFullYear());
}


// app initialization code...
await populateCalendars();
```

<br>

### __marsTime__

`marsTime(date)`

Returns calendar `year`, `period`, `period week`, `week`, `day`, `shift`, and `batch code` for the date specified. If the time is included with the date it will calculate the day (A, B, C, etc), shift and batch codes based on the time.

If the date does not include a time stamp `00:00:00` will be used. In this case, the `day`, `shift` and `batch codes` will not be accurate for the date.

Calendar data must be loaded before calling  `marsTime` otherwise an error will be returned.


```javascript
const { marsTime } = require('mars-time');

const { year, period, pweek, week, day, shift, batch } = await marsTime(date);

// do something with the data
```
